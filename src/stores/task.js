import { ref } from 'vue'
import { defineStore } from 'pinia'

export const useTaskStore = defineStore('task', () => {
  const todoList = ref([])
  const taskDetail = ref(null)

  function addTask(task) {
    todoList.value.push(task)
  }

  function setDetail(taskId) {
    taskDetail.value = todoList.value.find((elm) => elm.id == taskId) || null
  }

  function switchStatus(taskId) {
    const idxMatch = todoList.value.findIndex((elm) => elm.id == taskId)

    if (idxMatch >= 0) {
      const newStatus = todoList.value[idxMatch] == 'TODO' ? 'TODO' : 'DONE'
      todoList.value[idxMatch].status = newStatus
      todoList.value[idxMatch].closed = new Date().getTime()
    }
  }

  function deteleTask(taskId) {
    const idxMatch = todoList.value.findIndex((elm) => elm.id == taskId)
    if (idxMatch >= 0) {
      todoList.value = todoList.value.filter((elm) => elm.id !== taskId)
    }
  }

  return { todoList, addTask, setDetail, switchStatus, deteleTask }
})
